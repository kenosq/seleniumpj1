from selenium.webdriver.chrome.options import Options
from selenium import webdriver
from datetime import datetime
from apscheduler.schedulers.background import BackgroundScheduler

import time

scheduler = BackgroundScheduler()
scheduler.daemonic = False
url = "http://34.229.24.67/"
options = Options()
options.add_argument("--headless")
options.add_argument("window-size=1400,1500")
options.add_argument("--disable-gpu")
options.add_argument("--no-sandbox")
options.add_argument("start-maximized")
options.add_argument("enable-automation")
options.add_argument("--disable-infobars")
options.add_argument("--disable-dev-shm-usage")
driver = webdriver.Chrome(options=options)

def url_title():
	driver.get(url)
	element_text = driver.find_element_by_id("currency").text 
	print("Element Text is", driver.title)
	print("Element currency", element_text)
	print(f'Tick! The time is: {datetime.now()}')

if __name__ == '__main__':
	scheduler = BackgroundScheduler()
	scheduler.add_job(url_title, 'interval', seconds=15)
	scheduler.start()
	try:
		while True:
			time.sleep(2)
			print('Printing in the main thread.')
	except KeyboardInterrupt:
		pass
	


